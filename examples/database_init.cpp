#include <eloquentxx/eloquentxx.hpp>
#include <iostream>

using namespace eloquentxx;

class V1Migration : public Migration
{
  public:
    V1Migration()
      : Migration("v1_migration")
    {}

    bool up()
    {
        return Schema::createIfNotExists("users", [](Blueprint& table) {
            table.increments("id");
            table.string("name");
            table.string("email").unique();
            table.string("password");
            table.string("avatar_url").nullable();
            table._enum("role", { "BACKEND", "ADMIN", "ENGINEER", "PILOT" })._default("PILOT");
            table.timestamps();
        });
    }

    bool down() { return Schema::dropIfExists("users"); }
};

int main()
{
    DB::initialize("sqlite3", "database.sqlite");
    if (DB::isOpen()) {
        std::cout << "Initialize: OK\n";
    }

    DB::addMigration(new V1Migration);

    if (DB::migrate()) {
        std::cout << "Migrate: OK\n";
    }
    return 0;
}