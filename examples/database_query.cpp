#include <eloquentxx/eloquentxx.hpp>
#include <iostream>

using namespace eloquentxx;

class V1Migration : public Migration
{
  public:
    V1Migration()
      : Migration("v1_migration")
    {}

    bool up()
    {
        return Schema::createIfNotExists("users", [](Blueprint& table) {
            table.increments("id");
            table.string("name");
            table.string("email").unique();
            table.string("password");
            table.string("avatar_url").nullable();
            table._enum("role", { "BACKEND", "ADMIN", "ENGINEER", "PILOT" })._default("PILOT");
            table.timestamps();
        });
    }

    bool down() { return Schema::dropIfExists("users"); }
};

int main()
{
    DB::initialize("sqlite3", ":memory:");
    if (DB::isOpen()) {
        std::cout << "Initialize: OK\n";
    }

    DB::addMigration(new V1Migration);

    if (DB::migrate()) {
        std::cout << "Migrate: OK\n";
    }

    //
    auto table_info = Schema::info("users");
    for (auto& ci : table_info) {
        std::cout << "C: " << ci.name << ": " << ci.type << std::endl;
    }

    DB::raw("INSERT INTO users(name,email,password) VALUES('Test #1','test@test1.com', 'password');");
    DB::raw("INSERT INTO users(name,email,password) VALUES('Test #2','test@test2.com', 'password');");

    std::cout << "---\n";
    auto items = DB::table("users").orderBy("id", "desc").get();
    for (auto& i : items) {
        std::cout << "User { \n";
        for (auto& key : i.keys()) {
            std::cout << "   " << key << ": " << i.get(key) << std::endl;
        }
        std::cout << "}\n";
    }

    std::cout << "---\n";
    auto item = DB::table("users").first();

    return 0;
}