#include <eloquentxx/collection.hpp>

using namespace eloquentxx;

Collection::operator bool() const
{
    return !!size();
}

Model& Collection::first()
{
    return front();
}

Model& Collection::last()
{
    return back();
}