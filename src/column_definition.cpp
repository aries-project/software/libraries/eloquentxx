#include <eloquentxx/column_definition.hpp>

using namespace eloquentxx;

ColumnDefinition::ColumnDefinition(const std::string& name, const std::string& _type, const std::vector<unsigned>& size)
{
    m_name    = name;
    m_type    = _type;
    m_size    = size;
    m_foreign = (_type == "FOREIGN");
}

ColumnDefinition& ColumnDefinition::_default(const std::string& value)
{
    m_default       = true;
    m_default_value = "'" + value + "'";
    return *this;
}

ColumnDefinition& ColumnDefinition::_default(int value)
{
    m_default       = true;
    m_default_value = std::to_string(value);
    return *this;
}

ColumnDefinition& ColumnDefinition::_default(double value)
{
    m_default       = true;
    m_default_value = std::to_string(value);
    return *this;
}

ColumnDefinition& ColumnDefinition::_unsigned(bool value)
{
    /* add your implementation here */
    return *this;
}

ColumnDefinition& ColumnDefinition::nullable(bool value)
{
    m_nullable = value;
    return *this;
}

ColumnDefinition& ColumnDefinition::unique(bool value)
{
    m_unique = value;
    return *this;
}

ColumnDefinition& ColumnDefinition::index(bool value)
{
    m_index = value;
    return *this;
}

ColumnDefinition& ColumnDefinition::primary(bool value)
{
    m_primary = value;
    return *this;
}

ColumnDefinition& ColumnDefinition::autoIncrement(bool value)
{
    m_autoincrement = value;
    return *this;
}

ColumnDefinition& ColumnDefinition::on(const std::string& table)
{
    m_foreign       = true;
    m_foreign_table = table;
    return *this;
}

ColumnDefinition& ColumnDefinition::onDelete(ForeignKeyAction action)
{
    std::string action_str;
    switch (action) {
        case ForeignKeyAction::NO_ACTION:
            action_str = "NO ACTION";
            break;
        case ForeignKeyAction::RESTRICT:
            action_str = "RESTRICT";
            break;
        case ForeignKeyAction::SET_NULL:
            action_str = "SET NULL";
            break;
        case ForeignKeyAction::SET_DEFAULT:
            action_str = "SET DEFAULT";
            break;
        case ForeignKeyAction::CASCADE:
            action_str = "CASCADE";
            break;
    }
    m_on_delete = action_str;
    return *this;
}

ColumnDefinition& ColumnDefinition::onUpdate(ForeignKeyAction action)
{
    std::string action_str;
    switch (action) {
        case ForeignKeyAction::NO_ACTION:
            action_str = "NO ACTION";
            break;
        case ForeignKeyAction::RESTRICT:
            action_str = "RESTRICT";
            break;
        case ForeignKeyAction::SET_NULL:
            action_str = "SET NULL";
            break;
        case ForeignKeyAction::SET_DEFAULT:
            action_str = "SET DEFAULT";
            break;
        case ForeignKeyAction::CASCADE:
            action_str = "CASCADE";
            break;
    }
    m_on_update = action_str;
    return *this;
}

ColumnDefinition& ColumnDefinition::references(const std::string& column)
{
    m_foreign            = true;
    m_foreign_references = column;
    return *this;
}

ColumnDefinition& ColumnDefinition::check(CheckAction action, const std::vector<std::string>& values)
{
    m_checks.push_back(CheckExpression{ action, 0, values });
    return *this;
}

ColumnDefinition& ColumnDefinition::check(CheckAction action, int value)
{
    m_checks.push_back(CheckExpression{ action, value });
    return *this;
}

std::string ColumnDefinition::getName() const
{
    return m_name;
}

std::string ColumnDefinition::getType() const
{
    return m_type;
}

const std::vector<unsigned>& ColumnDefinition::getSize() const
{
    return m_size;
}

bool ColumnDefinition::getIndex() const
{
    return m_index;
}

bool ColumnDefinition::getAutoincrement() const
{
    return m_autoincrement;
}

bool ColumnDefinition::getNullable() const
{
    return m_nullable;
}

bool ColumnDefinition::getPrimary() const
{
    return m_primary;
}

bool ColumnDefinition::getUnique() const
{
    return m_unique;
}

bool ColumnDefinition::getDefault() const
{
    return m_default;
}

std::string ColumnDefinition::getDefaultValue() const
{
    return m_default_value;
}

bool ColumnDefinition::getForeign() const
{
    return m_foreign;
}

std::string ColumnDefinition::getForeignTable() const
{
    return m_foreign_table;
}

std::string ColumnDefinition::getForeignReferences() const
{
    return m_foreign_references;
}

std::string ColumnDefinition::getOnDelete() const
{
    return m_on_delete;
}

std::string ColumnDefinition::getOnUpdate() const
{
    return m_on_update;
}

const std::vector<CheckExpression>& ColumnDefinition::getChecks() const
{
    return m_checks;
}
