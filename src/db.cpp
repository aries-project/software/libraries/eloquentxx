#include "eloquentxx/db.hpp"
#include "eloquentxx/migration.hpp"
#include <SQLiteCpp/Database.h>
#include <SQLiteCpp/Statement.h>
#include <SQLiteCpp/Transaction.h>
#include <chrono>
#include <iomanip>
#include <iostream>

using namespace eloquentxx;

bool DB::m_open = false;
std::string DB::m_driver("undefined");
std::unique_ptr<SQLite::Database> DB::m_database(nullptr);
std::vector<std::unique_ptr<Migration>> DB::m_migrations;

bool DB::initialize(const std::string& driver, const std::string& database)
{
    try {
        if (driver != "sqlite3") {
            throw std::runtime_error("Supported drivers are: \"sqlite3\"");
        }
        m_database = std::make_unique<SQLite::Database>(database, SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE);
        m_driver   = driver;
        m_open     = true;
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return false;
    }
    return true;
}

bool DB::isOpen()
{
    return m_open;
}

std::string DB::lastErrorMessage()
{
    if (m_open) {
        return m_database->getErrorMsg();
    }
    return "";
}

bool DB::createMigrationsTable()
{
    return Schema::createIfNotExists("migrations", [](Blueprint& table) {
        table.increments("id");
        table.string("migration");
        table.integer("batch");
    });
}

void DB::addMigration(Migration* migration)
{
    m_migrations.emplace_back(migration);
}

bool DB::migrate()
{
    bool status       = createMigrationsTable();
    int current_batch = 0;
    int next_batch    = 0;

    SQLite::Statement query(*m_database, "SELECT * from migrations");
    while (query.executeStep()) {
        std::string migration = query.getColumn(1);
        int batch             = query.getColumn(2);

        for (auto& m : m_migrations) {
            if (m->getName() == migration) {
                m->setBatch(batch);
            }
        }

        if (batch > current_batch)
            current_batch = batch;
    }

    next_batch = current_batch + 1;

    for (auto& m : m_migrations) {
        if (m->getBatch() > 0)
            continue;

        std::cout << "Migration " << m->getName() << std::endl;

        SQLite::Transaction transaction(*m_database);

        status &= m->up();

        if (status) {

            SQLite::Statement mark(*m_database, "INSERT INTO migrations(migration,batch) VALUES(?,?)");
            mark.bind(1, m->getName());
            mark.bind(2, next_batch);
            mark.exec();

            transaction.commit();
        } else {
            break;
        }
    }

    return status;
}

bool DB::rollback()
{
    bool status       = createMigrationsTable();
    int current_batch = 0;

    SQLite::Statement query(*m_database, "SELECT * from migrations");
    while (query.executeStep()) {
        std::string migration = query.getColumn(1);
        int batch             = query.getColumn(2);

        for (auto& m : m_migrations) {
            if (m->getName() == migration) {
                m->setBatch(batch);
            }
        }

        if (batch > current_batch)
            current_batch = batch;
    }

    if (current_batch == 0) {
        std::cout << "Nothing to do!\n";
        return true;
    }

    for (auto& m : m_migrations) {
        if (m->getBatch() != current_batch) {
            std::cout << "  skipping\n";
            break;
        }

        std::cout << "Rolling back " << m->getName() << std::endl;

        SQLite::Transaction transaction(*m_database);

        status &= m->down();

        if (status) {

            SQLite::Statement mark(*m_database, "DELETE FROM migrations WHERE migration = ?");
            mark.bind(1, m->getName());
            mark.exec();

            transaction.commit();
        } else {
            break;
        }
    }

    return status;
}

bool DB::raw(const std::string& stmt)
{
    try {
        m_database->exec(stmt);
        return true;
    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return false;
    }
}

SQLite::Statement DB::query(const std::string& stmt)
{
    return SQLite::Statement(*m_database, stmt);
}

Builder DB::table(const std::string& table_name)
{
    return Builder(table_name);
}

std::string DB::sqlTime(int relativeSeconds)
{
    auto now = std::chrono::system_clock::now();
    now += std::chrono::seconds(relativeSeconds);

    auto itt = std::chrono::system_clock::to_time_t(now);
    std::ostringstream ss;
    ss << std::put_time(localtime(&itt), "%FT%T");
    return ss.str();
}