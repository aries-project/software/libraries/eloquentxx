#include <SQLiteCpp/Column.h>
#include <SQLiteCpp/Statement.h>
#include <eloquentxx/builder.hpp>
#include <eloquentxx/db.hpp>
#include <iostream>

using namespace eloquentxx;

Builder::Builder(const std::string& table)
  : m_table(table)
{}

std::string Builder::escapeKey(const std::string& key)
{
    return "`" + m_table + "`.`" + key + "`";
}

std::string Builder::escapeValue(const std::string& value)
{
    return value;
}

Builder& Builder::take(int limit)
{
    m_limit = limit;
    return *this;
}

Builder& Builder::skip(int offset)
{
    m_offset = offset;
    return *this;
}

Builder& Builder::where(const std::string& key,
                        const std::string& op,
                        const std::string& value,
                        const std::string& boolean)
{
    if (!m_where_caluse.empty())
        m_where_caluse += boolean;

    m_where_caluse += escapeKey(key) + " " + op + " " + escapeValue(value);

    return *this;
}

Builder& Builder::where(const std::string& key, const std::string& value)
{
    return where(key, "=", value);
}

Builder& Builder::orderBy(const std::string& column, const std::string& order, bool nulls_last)
{
    std::string order_clause = column + " " + order;
    if (nulls_last)
        order_clause += " NULLS LAST";
    m_order_by_clause.push_back(order_clause);
    return *this;
}

Collection Builder::get(const std::string& column) const
{
    std::string stmt = "SELECT " + column + " FROM " + m_table;
    if (!m_where_caluse.empty())
        stmt += " WHERE " + m_where_caluse;

    if (!m_group_by_clause.empty())
        stmt += " GROUP BY " + m_group_by_clause;

    if (!m_having_clause.empty())
        stmt += " HAVING " + m_having_clause;

    if (m_order_by_clause.size() > 0) {
        stmt += " ORDER BY ";
        bool comma = false;
        for (auto& order : m_order_by_clause) {
            if (comma)
                stmt += ", ";

            stmt += order;

            if (!comma)
                comma = true;
        }
    }

    if (m_offset)
        stmt += " LIMIT " + std::to_string(m_offset) + ", " + std::to_string(m_limit);
    else if (m_limit)
        stmt += " LIMIT " + std::to_string(m_limit);

    stmt += ";";

    Collection collection;

    auto query = DB::query(stmt);
    while (query.executeStep()) {
        Model m;

        for (int i = 0; i < query.getColumnCount(); i++) {
            auto colname      = query.getColumnName(i);
            auto column       = query.getColumn(i);
            std::string value = column.getString();
            m.set(colname, value);
        }
        m.saved();
        collection.push_back(m);
    }
    return collection;
}

Model Builder::first()
{
    Collection collection = take(1).get();
    return collection.size() ? collection.first() : Model();
}