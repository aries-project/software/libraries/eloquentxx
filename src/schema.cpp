#include <SQLiteCpp/Column.h>
#include <SQLiteCpp/Statement.h>
#include <eloquentxx/db.hpp>
#include <eloquentxx/schema.hpp>

using namespace eloquentxx;

std::vector<ColumnInfo> Schema::info(const std::string& table)
{
    std::vector<ColumnInfo> result;
    std::string stmt = "PRAGMA table_info(" + table + ");";
    auto query       = DB::query(stmt);
    while (query.executeStep()) {
        int cid                = query.getColumn(0);
        std::string name       = query.getColumn(1);
        std::string type       = query.getColumn(2);
        int not_null           = query.getColumn(3);
        std::string dflt_value = query.getColumn(4);
        int pk                 = query.getColumn(5);

        result.push_back({ cid, name, type, (bool)not_null, dflt_value, pk });
    }

    return result;
}

bool Schema::create(const std::string& table, const CreateTableCallback& callback)
{
    Blueprint bp;
    callback(bp);
    std::string stmt = "CREATE TABLE \"" + table + "\" (";

    bool comma = false;
    for (auto& col : bp.getColumns()) {
        auto col_stmt = Schema::toSql(col);

        if (comma)
            stmt += ", ";

        stmt += col_stmt;

        if (!comma)
            comma = true;
    }
    stmt += ");";
    return DB::raw(stmt);
}

bool Schema::createIfNotExists(const std::string& table, const CreateTableCallback& callback)
{
    Blueprint bp;
    callback(bp);
    std::string stmt = "CREATE TABLE IF NOT EXISTS \"" + table + "\" (";

    bool comma = false;
    for (auto& col : bp.getColumns()) {
        auto col_stmt = Schema::toSql(col);

        if (comma)
            stmt += ", ";

        stmt += col_stmt;

        if (!comma)
            comma = true;
    }
    stmt += ");";
    return DB::raw(stmt);
}

bool Schema::table(const std::string& table, const CreateTableCallback& callback)
{
    return false;
}

bool Schema::rename(const std::string& table, const std::string& new_table)
{
    std::string stmt = "ALTER TABLE \"" + table + "\" RENAME TO \"" + new_table + "\"";
    return DB::raw(stmt);
}

bool Schema::drop(const std::string& table)
{
    std::string stmt = "DROP TABLE \"" + table + "\";";
    return DB::raw(stmt);
}

bool Schema::dropIfExists(const std::string& table)
{
    std::string stmt = "DROP TABLE IF EXISTS \"" + table + "\";";
    return DB::raw(stmt);
}

bool Schema::enableForeignKeyConstraints()
{
    return false;
}

bool Schema::disableForeignKeyConstraints()
{
    return false;
}

std::string Schema::toSql(const ColumnDefinition& def)
{
    std::string statment;

    if (!def.getForeign()) {
        statment = "\"" + def.getName() + "\" " + def.getType();
        if (!def.getNullable()) {
            statment += " NOT NULL";
        }
        if (def.getDefault()) {
            statment += " DEFAULT " + def.getDefaultValue();
        }
        if (def.getPrimary()) {
            statment += " PRIMARY KEY";
        }
        if (def.getAutoincrement()) {
            statment += " AUTOINCREMENT";
        }
    } else {
        statment = "FOREIGN KEY(\"" + def.getName() + "\")";

        if (!def.getForeignTable().empty() && !def.getForeignReferences().empty()) {
            statment += " REFERENCES \"" + def.getForeignTable() + "\"";
            statment += "(\"" + def.getForeignReferences() + "\")";
        } else {
            // TODO: error handling
        }

        if (!def.getOnDelete().empty()) {
            statment += " ON DELETE " + def.getOnDelete();
        }

        if (!def.getOnUpdate().empty()) {
            statment += " ON DELETE " + def.getOnUpdate();
        }
    }

    auto& checks = def.getChecks();
    if (checks.size() > 0) {
        //
        for (auto& check : checks) {
            if (check.action == CheckAction::IN_VALUES) {
                std::string expr;

                expr += "\"" + def.getName() + "\" IN (";
                bool comma = false;
                for (auto& v : check.values) {
                    if (comma) {
                        expr += ", ";
                    }
                    expr += "'" + v + "'";
                    if (!comma) {
                        comma = true;
                    }
                }
                expr += ")";

                statment += " CHECK(" + expr + ")";
            }
        }
    }

    return statment;
}
