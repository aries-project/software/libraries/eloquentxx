#include "eloquentxx/migration.hpp"

namespace eloquentxx {

Migration::Migration(const std::string& name)
  : m_name(name)
{}

Migration::~Migration() {}

bool Migration::up()
{
    return true;
}

bool Migration::down()
{
    return true;
}

void Migration::setName(const std::string& value)
{
    m_name = value;
}

std::string Migration::getName() const
{
    return m_name;
}

void Migration::setBatch(int value)
{
    m_batch = value;
}

int Migration::getBatch() const
{
    return m_batch;
}

} // namespace eloquentxx
