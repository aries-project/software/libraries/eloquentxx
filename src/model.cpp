#include <eloquentxx/db.hpp>
#include <eloquentxx/model.hpp>

using namespace eloquentxx;

Model::Model() {}

Model::Model(const std::unordered_map<std::string, std::string>& data)
  : m_data(data)
  , m_original(data)
{}

Model::~Model() {}

std::string Model::get(const std::string& key) const
{
    return m_data.at(key);
}

int Model::getInt(const std::string& key) const
{
    return std::stoi(m_data.at(key));
}

void Model::set(const std::string& key, const std::string& value)
{
    m_data[key] = value;
}

int Model::id() const
{
    return getInt("id");
}

std::vector<std::string> Model::keys() const
{
    std::vector<std::string> result;
    for (auto& attr : m_data) {
        result.push_back(attr.first);
    }
    return result;
}

std::string Model::created_at() const
{
    return m_created_at;
}

std::string Model::updated_at() const
{
    return m_updated_at;
}

bool Model::useTimestamps() const
{
    return m_timestamps;
}

void Model::touch()
{
    if (useTimestamps())
        set(updated_at(), DB::sqlTime());
}

void Model::saved()
{
    m_exists   = true;
    m_original = m_data;
}

Model::operator bool() const
{
    return m_data.size() || m_original.size();
}