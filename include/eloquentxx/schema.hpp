#pragma once

#include <eloquentxx/blueprint.hpp>
#include <functional>

namespace eloquentxx {

typedef std::function<void(Blueprint& table)> CreateTableCallback;

struct ColumnInfo
{
    int cid;
    std::string name;
    std::string type;
    bool not_null;
    std::string default_value;
    int primary_key;
};

/**
 *
 */
class Schema
{
  public:
    /**
     *
     * @param[in] table
     * @return
     */
    static std::vector<ColumnInfo> info(const std::string& table);

    /**
     *
     * @param[in] table
     * @param[in] callback
     * @return
     */
    static bool create(const std::string& table, const CreateTableCallback& callback);

    /**
     *
     * @param[in] table
     * @param[in] callback
     * @return
     */
    static bool createIfNotExists(const std::string& table, const CreateTableCallback& callback);

    /**
     *
     * @param[in] table
     * @param[in] new_table
     * @return
     */
    static bool rename(const std::string& table, const std::string& new_table);

    /**
     *
     * @param[in] table
     * @return
     */
    static bool drop(const std::string& table);

    /**
     *
     * @param[in] table
     * @return
     */
    static bool dropIfExists(const std::string& table);

    /**
     *
     * @param[in] table
     * @param[in] callback
     * @return
     */
    static bool table(const std::string& table, const CreateTableCallback& callback);

    /**
     *
     * @return
     */
    static bool enableForeignKeyConstraints();

    /**
     *
     * @return
     */
    static bool disableForeignKeyConstraints();

  protected:
  private:
    /**
     *
     * @param[in] def
     * @return
     */
    static std::string toSql(const ColumnDefinition& def);
};

} // namespace eloquentxx
