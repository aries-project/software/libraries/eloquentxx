#pragma once

#include <eloquentxx/builder.hpp>
#include <eloquentxx/migration.hpp>
#include <memory>
#include <string>
#include <vector>

namespace SQLite {
class Database;
class Statement;
} // namespace SQLite

namespace eloquentxx {

class DB
{
  public:
    /**
     *
     *
     */
    static bool initialize(const std::string& driver, const std::string& database);

    /**
     *
     *
     */
    static bool isOpen();

    /**
     *
     */
    static std::string lastErrorMessage();

    /**
     *
     */
    static std::string sqlTime(int relativeSeconds = 0);

    /**
     *
     */
    static bool raw(const std::string& stmt);

    /**
     *
     */
    static SQLite::Statement query(const std::string& stmt);

    /**
     *
     */
    static Builder table(const std::string& table_name);

    /**
     *
     */
    static bool migrate();

    /**
     *
     */
    static bool rollback();

    /**
     *
     */
    static void addMigration(Migration* migration);

  protected:
  private:
    static bool m_open;
    static std::string m_driver;
    static std::unique_ptr<SQLite::Database> m_database;
    static std::vector<std::unique_ptr<Migration>> m_migrations;

    static bool createMigrationsTable();
};

} // namespace eloquentxx
