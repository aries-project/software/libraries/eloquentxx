#pragma once

#include <eloquentxx/model.hpp>
#include <list>

namespace eloquentxx {

class Collection : public std::list<Model>
{
  public:
    operator bool() const;

    Model& first();
    Model& last();
};

} // namespace eloquentxx