#pragma once
#include <eloquentxx/column_definition.hpp>

namespace eloquentxx {

/**
 *
 */
class Blueprint
{
    // section:manual_definitions
    // endsection:manual_definitions

  public:
    /**
     * The increments method creates an auto-incrementing UNSIGNED INTEGER equivalent column as a primary key.
     * @param[in] column
     * @return
     */
    ColumnDefinition& increments(const std::string& column);

    /**
     * The integer method creates an INTEGER equivalent column.
     * @param[in] column
     * @return
     */
    ColumnDefinition& integer(const std::string& column);

    /**
     * The float method creates a FLOAT equivalent column with the given precision (total digits) and scale (decimal
     * digits).
     * @param[in] column
     * @param[in] digits
     * @param[in] decimals
     * @return
     */
    ColumnDefinition& _float(const std::string& column, unsigned digits = 8, unsigned decimals = 2);

    /**
     * The double method creates a DOUBLE equivalent column with the given precision (total digits) and scale
     * (decimal digits).
     * @param[in] column
     * @param[in] digits
     * @param[in] decimals
     * @return
     */
    ColumnDefinition& _double(const std::string& column, unsigned digits = 8, unsigned decimals = 2);

    /**
     * The decimal method creates a DECIMAL equivalent column with the given precision (total digits) and scale
     * (decimal digits).
     * @param[in] column
     * @param[in] digits
     * @param[in] decimals
     * @return
     */
    ColumnDefinition& decimal(const std::string& column, unsigned digits = 8, unsigned decimals = 2);

    /**
     * The boolean method creates a BOOLEAN equivalent column.
     * @param[in] column
     * @return
     */
    ColumnDefinition& boolean(const std::string& column);

    /**
     * The string method creates a VARCHAR equivalent column of the given length.
     * @param[in] column
     * @param[in] length
     * @return
     */
    ColumnDefinition& string(const std::string& column, unsigned length = 0);

    /**
     * The text method creates a TEXT equivalent column.
     * @param[in] column
     * @return
     */
    ColumnDefinition& text(const std::string& column);

    /**
     * The enum method creates a ENUM equivalent column with the given valid values
     * @param[in] column
     * @param[in] values
     * @return
     */
    ColumnDefinition& _enum(const std::string& column, const std::vector<std::string>& values);

    /**
     * The timestamp method creates a TIMESTAMP equivalent column.
     * @param[in] column
     * @return
     */
    ColumnDefinition& timestamp(const std::string& column);

    /**
     * The timestamps method creates created_at and updated_at TIMESTAMP equivalent columns.
     */
    void timestamps();

    /**
     * The foreign method creates a FOREIGN KEY contraint.
     * @param[in] column
     * @return
     */
    ColumnDefinition& foreign(const std::string& column);

    /**
     * The foreignId method is an alias of the unsignedBigInteger method.
     * @param[in] column
     * @return
     */
    ColumnDefinition& foreignId(const std::string& column);

    /**
     * Get column definitions
     * @return
     */
    const std::vector<ColumnDefinition>& getColumns() const;

  protected:
  private:
    /// Column definitions
    std::vector<ColumnDefinition> m_columns;
};

} // namespace eloquentxx
