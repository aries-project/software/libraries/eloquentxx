#pragma once

#include <eloquentxx/collection.hpp>
#include <eloquentxx/model.hpp>
#include <list>
#include <string>

namespace eloquentxx {

class Builder
{
  public:
    explicit Builder(const std::string& table);

    Builder& where(const std::string& key,
                   const std::string& op,
                   const std::string& value,
                   const std::string& boolean = "and");
    Builder& where(const std::string& key, const std::string& value);

    Builder& orderBy(const std::string& column, const std::string& order = "asc", bool nulls_last = false);

    // Builder& latest();

    Builder& take(int limit);
    Builder& skip(int offset);

    Collection get(const std::string& column = "*") const;
    Model first();

  protected:
  private:
    std::string m_table;

    std::string m_where_caluse;
    std::vector<std::string> m_order_by_clause;
    std::string m_group_by_clause;
    std::string m_having_clause;

    int m_limit  = 0;
    int m_offset = 0;

    std::string escapeKey(const std::string& key);
    std::string escapeValue(const std::string& value);
};

} // namespace eloquentxx