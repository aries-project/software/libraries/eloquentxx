#pragma once

#include <string>
#include <vector>

namespace eloquentxx {

/**
 *
 */
enum class ForeignKeyAction
{
    NO_ACTION,
    RESTRICT,
    SET_NULL,
    SET_DEFAULT,
    CASCADE,
};

/**
 *
 */
enum class CheckAction
{
    IN_VALUES,
};

/**
 *
 */
struct CheckExpression
{
    /// Check expression action
    CheckAction action = CheckAction::IN_VALUES;
    /// Expression value
    int value = 0;
    /// Expression values
    std::vector<std::string> values = {};
};

/**
 *
 */
class ColumnDefinition
{
  public:
    /**
     *
     * @param[in] name
     * @param[in] _type
     * @param[in] size
     */
    ColumnDefinition(const std::string& name, const std::string& _type, const std::vector<unsigned>& size = {});

    /**
     *
     * @param[in] value
     * @return
     */
    ColumnDefinition& _default(const std::string& value);

    /**
     *
     * @param[in] value
     * @return
     */
    ColumnDefinition& _default(int value);

    /**
     *
     * @param[in] value
     * @return
     */
    ColumnDefinition& _default(double value);

    /**
     *
     * @param[in] value
     * @return
     */
    ColumnDefinition& _unsigned(bool value = true);

    /**
     *
     * @param[in] value
     * @return
     */
    ColumnDefinition& nullable(bool value = true);

    /**
     *
     * @param[in] value
     * @return
     */
    ColumnDefinition& unique(bool value = true);

    /**
     *
     * @param[in] value
     * @return
     */
    ColumnDefinition& index(bool value = true);

    /**
     *
     * @param[in] value
     * @return
     */
    ColumnDefinition& primary(bool value = true);

    /**
     *
     * @param[in] value
     * @return
     */
    ColumnDefinition& autoIncrement(bool value = true);

    /**
     *
     * @param[in] table
     * @return
     */
    ColumnDefinition& on(const std::string& table);

    /**
     *
     * @param[in] action
     * @return
     */
    ColumnDefinition& onDelete(ForeignKeyAction action);

    /**
     *
     * @param[in] action
     * @return
     */
    ColumnDefinition& onUpdate(ForeignKeyAction action);

    /**
     *
     * @param[in] column
     * @return
     */
    ColumnDefinition& references(const std::string& column);

    /**
     *
     * @param[in] action
     * @param[in] values
     * @return
     */
    ColumnDefinition& check(CheckAction action, const std::vector<std::string>& values);

    /**
     *
     * @param[in] action
     * @param[in] value
     * @return
     */
    ColumnDefinition& check(CheckAction action, int value);

    /**
     * Get name value
     * @return
     */
    std::string getName() const;

    /**
     * Get type value
     * @return
     */
    std::string getType() const;

    /**
     * Get size value
     * @return
     */
    const std::vector<unsigned>& getSize() const;

    /**
     * Get index value
     * @return
     */
    bool getIndex() const;

    /**
     * Get autoincrement value
     * @return
     */
    bool getAutoincrement() const;

    /**
     * Get nullable value
     * @return
     */
    bool getNullable() const;

    /**
     * Get primary value
     * @return
     */
    bool getPrimary() const;

    /**
     * Get unique value
     * @return
     */
    bool getUnique() const;

    /**
     * Get default value
     * @return
     */
    bool getDefault() const;

    /**
     * Get default_value value
     * @return
     */
    std::string getDefaultValue() const;

    /**
     * Get foreign value
     * @return
     */
    bool getForeign() const;

    /**
     * Get foreign_table value
     * @return
     */
    std::string getForeignTable() const;

    /**
     * Get foreign_references value
     * @return
     */
    std::string getForeignReferences() const;

    /**
     * Get on_delete value
     * @return
     */
    std::string getOnDelete() const;

    /**
     * Get on_update value
     * @return
     */
    std::string getOnUpdate() const;

    /**
     * Get checks value
     * @return
     */
    const std::vector<CheckExpression>& getChecks() const;

  protected:
  private:
    /// Column name
    std::string m_name;
    /// Column type
    std::string m_type;
    /// Column type sizes
    std::vector<unsigned> m_size = {};
    /// Column index state
    bool m_index = false;
    /// Column auto_increment state
    bool m_autoincrement = false;
    /// Column nullable state
    bool m_nullable = false;
    /// Column primary state
    bool m_primary = false;
    /// Column unique constraint state
    bool m_unique = false;
    /// Column default value state
    bool m_default = false;
    /// Column default value
    std::string m_default_value;
    /// Column foreign state
    bool m_foreign = false;
    /// Column foreign table reference
    std::string m_foreign_table;
    /// Column foreign table.column reference
    std::string m_foreign_references;
    /// Column on delete action
    std::string m_on_delete;
    /// Column on update action
    std::string m_on_update;
    /// Column check constraints
    std::vector<CheckExpression> m_checks;
};

} // namespace eloquentxx
