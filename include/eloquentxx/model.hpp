#pragma once
#include <string>
#include <unordered_map>
#include <vector>

namespace eloquentxx {

class Model
{
    friend class Builder;

  public:
    Model();
    Model(const std::unordered_map<std::string, std::string>& data);
    virtual ~Model();

    std::string get(const std::string& key) const;
    int getInt(const std::string& key) const;
    void set(const std::string& key, const std::string& value);

    virtual int id() const;

    std::vector<std::string> keys() const;

    virtual std::string created_at() const;
    virtual std::string updated_at() const;
    virtual bool useTimestamps() const;
    void touch();

    operator bool() const;

  protected:
    // Model data
    std::unordered_map<std::string, std::string> m_data;
    // Model original data
    std::unordered_map<std::string, std::string> m_original;

    // Timestamps state
    bool m_timestamps = true;
    // CreatedAt column name
    std::string m_created_at = "created_at";
    // UpdatedAt column name
    std::string m_updated_at = "updated_at";

  private:
    void saved();

    // true if model exists in the database
    bool m_exists = false;
};

} // namespace eloquentxx