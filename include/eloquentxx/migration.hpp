#pragma once

#include <eloquentxx/schema.hpp>
#include <string>

namespace eloquentxx {

/**
 *
 */
class Migration
{

  public:
    /**
     *
     * @param[in] name
     */
    Migration(const std::string& name);

    /**
     *
     */
    virtual ~Migration();

    /**
     * Run the migration.
     * @return
     */
    virtual bool up();

    /**
     * Reverse the migration.
     * @return
     */
    virtual bool down();

    /**
     * Set migration name
     * @param[in] value
     */
    void setName(const std::string& value);

    /**
     * Get migration name
     * @return
     */
    std::string getName() const;

    /**
     * Set migration batch
     * @param[in] value
     */
    void setBatch(int value);

    /**
     * Get migration batch
     * @return
     */
    int getBatch() const;

  protected:
  private:
    /// Migration name
    std::string m_name;
    /// Migration batch
    int m_batch = 0;
};

} // namespace eloquentxx
