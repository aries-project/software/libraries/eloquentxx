# This module tries to find eloquentxx library and include files
#
# ELOQUENTXX_INCLUDE_DIR, path where to find libwebsockets.h
# ELOQUENTXX_LIBRARY_DIR, path where to find libwebsockets.so
# ELOQUENTXX_LIBRARIES, the library to link against
# ELOQUENTXX_FOUND, If false, do not try to use libWebSockets
#
# This currently works probably only for Linux

FIND_PATH ( ELOQUENTXX_INCLUDE_DIR eloquentxx/eloquentxx.hpp
    /usr/local/include
    /usr/include
)

FIND_LIBRARY ( ELOQUENTXX_LIBRARIES eloquentxx
    /usr/local/lib
    /usr/lib
)

GET_FILENAME_COMPONENT( ELOQUENTXX_LIBRARY_DIR ${ELOQUENTXX_LIBRARIES} PATH )

SET ( ELOQUENTXX_FOUND "NO" )
IF ( ELOQUENTXX_INCLUDE_DIR )
    IF ( ELOQUENTXX_LIBRARIES )
        SET ( ELOQUENTXX_FOUND "YES" )
    ENDIF ( ELOQUENTXX_LIBRARIES )
ENDIF ( ELOQUENTXX_INCLUDE_DIR )

MARK_AS_ADVANCED(
    ELOQUENTXX_LIBRARY_DIR
    ELOQUENTXX_INCLUDE_DIR
    ELOQUENTXX_LIBRARIES
)
